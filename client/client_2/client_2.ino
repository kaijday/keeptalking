// Program: I2C slave sender template for multi-node Arduino I2C network
// Programmer: Hazim Bitar (techbitar.com)
// Date: March 30, 2014
// This example code is in the public domain.

#include <Wire.h>

#define NODE_ADDRESS 2  // Change this unique address for each I2C slave node
#define PAYLOAD_SIZE 3 // Number of bytes  expected to be received by the master I2C node

byte nodePayload[PAYLOAD_SIZE];

// Module pins
int activePin = 10;
int successPin = 11;
int failPin = 12;

// byte signal
int _active = 0;
int _success = 0;
int _fail = 0;

byte output;

void setup()
{

  Serial.begin(9600);  
  Serial.println("SLAVE SENDER NODE");
  Serial.print("Node address: ");
  Serial.println(NODE_ADDRESS);
  Serial.print("Payload size: ");
  Serial.println(PAYLOAD_SIZE);
  Serial.println("***********************");

  Wire.begin(NODE_ADDRESS);  // Activate I2C network
  Wire.onRequest(requestEvent); // Request attention of master node
  Wire.onReceive(receiveEvent); // Receive data from master

  pinMode(activePin, OUTPUT);
  pinMode(successPin, OUTPUT);
  pinMode(failPin, OUTPUT);
  digitalWrite(activePin, LOW);
  digitalWrite(successPin, LOW);
  digitalWrite(failPin, LOW);

}

void loop()
{ 
  delay(100);
  if(_active = 1)
  {
    nodePayload[0] = NODE_ADDRESS; // I am sending Node address back.  Replace with any other data 
    nodePayload[1] = _success; // Value of Success or Failure
    nodePayload[2] = _fail;
    
    moduleTask();
  }
}

// Put task logic in here
void moduleTask()
{
  int sensorValue = analogRead(A0)/4; 
  Serial.println(sensorValue);
  if(sensorValue > 200)
  {
    // If sensor value is High: Pass Module   
    _success = 1;
    digitalWrite(successPin, HIGH);
  }
  else if(sensorValue < 100 && _fail == 0)
  {
    // If sensor value is LOW: send failure
    _fail = 1;
    digitalWrite(failPin, HIGH);
  }
  else if(sensorValue > 100 )
  {
    // Do not keep sending failures, reset Failure to 0
    _fail = 0;
    digitalWrite(failPin, LOW);
  }
  
}

void requestEvent()
{
  Wire.write(nodePayload,PAYLOAD_SIZE);  
  Serial.print("Sensor value: ");   // for debugging purposes. 
  Serial.println(nodePayload[0]);
  Serial.println(nodePayload[1]);
  Serial.println(nodePayload[2]);   // for debugging purposes. 

  // Change fail value so it is only sent once 
  _fail = 2;
}

void receiveEvent(int activate)
{
  if(activate == 1)
  {
    _active = 1;
    digitalWrite(activePin, HIGH);
  }
}
